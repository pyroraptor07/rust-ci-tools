# Rust CI Tools

Docker image for use in Gitlab CI testing to reduce test time. Created for use in my own personal projects.

## Installed Tools

This image includes the following:
- The default Rust toolchain included in the official Rust docker image
- The nightly Rust toolchain
- The following additional OS packages:
    - lld
    - clang
    - pkg-config
    - libssl-dev
    - postgresql-client
- The following cargo plugins and rustup components (default toolchain unless otherwise specified):
    - clippy
    - rustfmt (nightly toolchain)
    - cargo-tarpaulin
    - cargo-audit
    - sqlx-cli v0.5.7 (postgres feature only)
