FROM rust:latest
RUN apt-get update && apt-get install -y lld clang pkg-config libssl-dev postgresql-client musl-tools musl-dev cmake build-essential
RUN cargo install --version=0.5.7 sqlx-cli --no-default-features --features postgres
RUN cargo install cargo-tarpaulin
RUN rustup component add clippy
RUN rustup toolchain install nightly --allow-downgrade --profile minimal
RUN rustup component add rustfmt --toolchain nightly
RUN rustup target add x86_64-unknown-linux-musl
RUN cargo install cargo-audit
